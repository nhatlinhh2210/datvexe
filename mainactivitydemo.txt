package com.example.datvexedemo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Consumer;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.datvexedemo.remote.ICloudFunctions;
import com.example.datvexedemo.remote.RetrofitICloudClient;
import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import dmax.dialog.SpotsDialog;
import io.reactivex.Scheduler;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.ResponseBody;


public class MainActivity extends AppCompatActivity {

    private static  int APP_REQUEST_CODE = 2210;
    private  FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener listener;
    private AlertDialog dialog;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ICloudFunctions cloudFunctions;



    @Override
    protected void onStart(){
        super.onStart();
        firebaseAuth.addAuthStateListener(listener);
    }

    @Override
    protected void onStop() {
        if (listener != null){
            firebaseAuth.removeAuthStateListener(listener);
        }
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init(){
        firebaseAuth = firebaseAuth.getInstance();
        dialog = new SpotsDialog.Builder().setCancelable(false).setContext(this).build();
        cloudFunctions = RetrofitICloudClient.getInstance().create(ICloudFunctions.class);
        listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    //da co tai khoan dang dang nhap
                    Toast.makeText(MainActivity.this,"�� c� t�i kho?n dang dang nh?p",Toast.LENGTH_LONG).show();
                }
                else {
                    // chua co tai khoan dang nhap
                    AccessToken accessToken = AccountKit.getCurrentAccessToken();
                    if (accessToken != null){
                        getCustomToken(accessToken);
                    }
                    else {
                        phoneLogin();
                    }
                }

            }
        };
    }

    private void phoneLogin(){
        Intent intent = new Intent(MainActivity.this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,configurationBuilder.build());
        startActivityForResult(intent,APP_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_REQUEST_CODE){
            handleFacebookLoginResult(resultCode,data);
        }
    }

    private void handleFacebookLoginResult(int resultCode, Intent data) {
        AccountKitLoginResult result = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
        if (result.getError() != null){
            Toast.makeText(this, result.getError().getUserFacingMessage(), Toast.LENGTH_SHORT).show();
        }
        else if (result.wasCancelled() || resultCode == RESULT_CANCELED){
            finish();
        }
        else {
            if (result.getAccessToken() != null){
                getCustomToken(result.getAccessToken());
                Toast.makeText(this, "Dang nhap thanh cong", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void getCustomToken(AccessToken accessToken){
        dialog.show();
        compositeDisposable.add(cloudFunctions.getCustomToken(accessToken.getToken())
                .subscribeOn(Scheduler.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Consumer<ResponseBody>) responseBody -> {
                    String customToken = responseBody.string();
                    signInWithCustomToken(customToken);

                }, (Consumer<Throwable>) throwable -> {
                    dialog.dismiss();
                    Toast.makeText(this, ""+throwable.getMessage(), Toast.LENGTH_SHORT).show();
                })
        );
    }

    private void signInWithCustomToken(String customToken){
        dialog.dismiss();
        firebaseAuth.signInWithCustomToken(customToken)
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()){
                        Toast.makeText(this, "Authentication Firebase Failed", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}

package com.example.datvexedemo.callback;

import com.example.datvexedemo.Model.Ticket;

import java.util.List;

public interface CallbackListTicket {
    void OnListTicketLoadSuccess (List<Ticket> listTicket);
    void OnListTicketLoadFailed(String message);
}

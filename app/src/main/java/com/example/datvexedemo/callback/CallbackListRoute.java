package com.example.datvexedemo.callback;

import com.example.datvexedemo.Model.ListRouteModel;

import java.util.List;

public interface CallbackListRoute {
    void OnListRouteLoadSuccess (List<ListRouteModel> listRouteModels);
    void OnListRouteLoadFailed(String message);
}

package com.example.datvexedemo.callback;

import com.example.datvexedemo.Model.ListBusModel;

import java.util.List;

public interface CallbackListBus {
    void OnListBusLoadSuccess (List<ListBusModel> listBusModels);
    void OnListBusLoadFailed(String message);
}

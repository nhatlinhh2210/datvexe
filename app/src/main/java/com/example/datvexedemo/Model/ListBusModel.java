package com.example.datvexedemo.Model;

import java.util.List;

public class ListBusModel {
    private String Company,Serial,Type,Name,Route;
    private String Seat;


    public ListBusModel(String company,String seat, String serial, String type,String name,String route) {
        Company = company;
        Route = route;
        Seat = seat;
        Serial = serial;
        Type = type;
        Name = name;
    }


    public void setSeat(String seat) {
        Seat = seat;
    }


    public ListBusModel() {
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getSerial() {
        return Serial;
    }

    public String getRoute() {
        return Route;
    }

    public void setRoute(String route) {
        Route = route;
    }

    public void setSerial(String serial) {
        Serial = serial;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSeat() {
        return Seat;
    }
}



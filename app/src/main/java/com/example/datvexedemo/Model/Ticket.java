package com.example.datvexedemo.Model;

public class Ticket {
    private String name,Serial,DayStart,TimeStart,Price,Start, End;

    public Ticket() {
    }

    public Ticket(String name, String serial, String dayStart, String timeStart, String price, String start, String end) {
        this.name = name;
        Serial = serial;
        DayStart = dayStart;
        TimeStart = timeStart;
        Price = price;
        Start = start;
        End = end;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerial() {
        return Serial;
    }

    public void setSerial(String serial) {
        Serial = serial;
    }

    public String getDayStart() {
        return DayStart;
    }

    public void setDayStart(String dayStart) {
        DayStart = dayStart;
    }

    public String getTimeStart() {
        return TimeStart;
    }

    public void setTimeStart(String timeStart) {
        TimeStart = timeStart;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getStart() {
        return Start;
    }

    public void setStart(String start) {
        Start = start;
    }

    public String getEnd() {
        return End;
    }

    public void setEnd(String end) {
        End = end;
    }
}

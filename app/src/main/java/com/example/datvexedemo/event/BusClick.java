package com.example.datvexedemo.event;

import com.example.datvexedemo.Model.ListBusModel;

public class BusClick {
    private boolean success;
    private ListBusModel listBusModel;

    public BusClick(boolean success, ListBusModel listBusModel) {
        this.success = success;
        this.listBusModel = listBusModel;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ListBusModel getListBusModel() {
        return listBusModel;
    }

    public void setListBusModel(ListBusModel listBusModel) {
        this.listBusModel = listBusModel;
    }
}

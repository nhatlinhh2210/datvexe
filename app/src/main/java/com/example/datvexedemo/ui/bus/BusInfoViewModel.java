package com.example.datvexedemo.ui.bus;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.datvexedemo.Common.Common;
import com.example.datvexedemo.Model.ListBusModel;

public class BusInfoViewModel extends ViewModel {

    private MutableLiveData<ListBusModel> mutableLiveDataBus;

    public BusInfoViewModel(){

    }

    public MutableLiveData<ListBusModel> getMutableLiveDataBus(){
        if (mutableLiveDataBus == null){
            mutableLiveDataBus = new MutableLiveData<>();
        }
        mutableLiveDataBus.setValue(Common.BusSelected);
        return mutableLiveDataBus;
    }
}

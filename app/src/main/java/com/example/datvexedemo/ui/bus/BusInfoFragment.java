package com.example.datvexedemo.ui.bus;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.datvexedemo.Model.ListBusModel;
import com.example.datvexedemo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BusInfoFragment extends Fragment {

    private BusInfoViewModel busInfoViewModel;

    private Unbinder unbinder;

    @BindView(R.id.tv_busName)
    TextView tv_busName;

    @BindView(R.id.tv_busCompany)
    TextView tv_busCompany;

    @BindView(R.id.tv_busSerial)
    TextView tv_busSerial;

    @BindView(R.id.tv_routeName)
    TextView tv_routeName;

    @BindView(R.id.tv_seatNumber)
    TextView tv_seatNumber;

    @BindView(R.id.tv_seatType)
    TextView tv_seatType;

    @BindView(R.id.btn_addTicket)
    Button btn_addTicket;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        busInfoViewModel =
                ViewModelProviders.of(this).get(BusInfoViewModel.class);
        View root = inflater.inflate(R.layout.fragment_businfo, container, false);
        unbinder = ButterKnife.bind(this, root);

        busInfoViewModel.getMutableLiveDataBus().observe(this, listBusModel -> {
            displayInfo(listBusModel);
        });

        return root;
    }

    private void displayInfo(ListBusModel listBusModel) {
        tv_busName.setText(new StringBuilder(listBusModel.getName()));
        tv_busCompany.setText(new StringBuilder(listBusModel.getCompany()));
        tv_busSerial.setText(new StringBuilder(listBusModel.getSerial()));
        tv_routeName.setText(new StringBuilder(listBusModel.getRoute()));
        tv_seatNumber.setText(new StringBuilder(listBusModel.getSeat().toString()));
        tv_seatType.setText(new StringBuilder(listBusModel.getType()));

    }
}

package com.example.datvexedemo.ui.bus;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.datvexedemo.R;
import com.example.datvexedemo.adapter.ListBusAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BusFragment extends Fragment {

    private BusViewModel busViewModel;

    Unbinder unbinder;

    @BindView(R.id.recycler_busLookup)
    RecyclerView recycler_listbus;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        busViewModel =
                ViewModelProviders.of(this).get(BusViewModel.class);
        View root = inflater.inflate(R.layout.fragment_bus, container, false);

        unbinder = ButterKnife.bind(this, root);
        init();
        busViewModel.getBusList().observe(this,listBusModels -> {
            ListBusAdapter adapter = new ListBusAdapter(getContext(),listBusModels);
            recycler_listbus.setAdapter(adapter);

        });
        return root;
    }

    private void init() {
        recycler_listbus.setHasFixedSize(true);
        recycler_listbus.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
    }
}

package com.example.datvexedemo.ui.route;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.datvexedemo.Common.Common;
import com.example.datvexedemo.Model.ListRouteModel;

public class RouteinfoViewModel extends ViewModel {

    private MutableLiveData<ListRouteModel> mutableLiveDataRoute;

    public RouteinfoViewModel(){
    }

    public MutableLiveData<ListRouteModel> getMutableLiveDataRoute() {
        if (mutableLiveDataRoute == null){
            mutableLiveDataRoute = new MutableLiveData<>();
        }
        mutableLiveDataRoute.setValue(Common.RouteSelected);
        return mutableLiveDataRoute;
    }

    // TODO: Implement the ViewModel
}

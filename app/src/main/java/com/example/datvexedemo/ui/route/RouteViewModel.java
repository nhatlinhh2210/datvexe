package com.example.datvexedemo.ui.route;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.datvexedemo.Common.Common;
import com.example.datvexedemo.Model.ListBusModel;
import com.example.datvexedemo.Model.ListRouteModel;
import com.example.datvexedemo.Model.Ticket;
import com.example.datvexedemo.adapter.ListTicketAdapter;
import com.example.datvexedemo.callback.CallbackListRoute;
import com.example.datvexedemo.callback.CallbackListTicket;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.sql.Ref;
import java.util.ArrayList;
import java.util.List;

public class RouteViewModel extends ViewModel implements CallbackListTicket {

    private MutableLiveData<List<Ticket>> ticketList;
    private MutableLiveData<String> errorMessage;
    private CallbackListTicket callbackListTicket;

    public RouteViewModel(){
        callbackListTicket = this;
    }

    public MutableLiveData<List<Ticket>> getTicketList(){
        if (ticketList == null) {
            ticketList = new MutableLiveData<>();
            errorMessage = new MutableLiveData<>();
            loadTicketList();
        }
        return ticketList;
    }

    private void loadTicketList()
    {
        List<Ticket> templist = new ArrayList<>();
        DatabaseReference ticketRef = FirebaseDatabase.getInstance().getReference(Common.USER_REFERENCES).child(Common.currentUser.getPhone()).child("Ticket");
        ticketRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot routeSnapShot:dataSnapshot.getChildren()){
                    Ticket model = routeSnapShot.getValue(Ticket.class);
                    templist.add(model);
                }
                callbackListTicket.OnListTicketLoadSuccess(templist);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callbackListTicket.OnListTicketLoadFailed(databaseError.getMessage());
            }
        });
    }

    public MutableLiveData<String> getErrorMessage(){
        return getErrorMessage();
    }

    @Override
    public void OnListTicketLoadSuccess(List<Ticket> listTicket) {
        ticketList.setValue(listTicket);
    }

    @Override
    public void OnListTicketLoadFailed(String message) {
        errorMessage.setValue(message);
    }
}
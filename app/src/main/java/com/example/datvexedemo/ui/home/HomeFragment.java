package com.example.datvexedemo.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.datvexedemo.R;
import com.example.datvexedemo.adapter.ListRouteAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HomeFragment extends Fragment {


    Unbinder unbinder;

    RecyclerView recycler_routeShowup;

    private HomeViewModel homeViewModel;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        recycler_routeShowup = (RecyclerView) root.findViewById(R.id.recycler_routeShowup);
        init();
        unbinder = ButterKnife.bind(this, root);
        homeViewModel.getRouteList().observe(this,listRouteModels -> {
            ListRouteAdapter listRouteAdapter = new ListRouteAdapter(getContext(),listRouteModels);
            recycler_routeShowup.setAdapter(listRouteAdapter);
        });
        return root;
    }

    private void init() {
        recycler_routeShowup.setHasFixedSize(true);
        recycler_routeShowup.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
    }


}

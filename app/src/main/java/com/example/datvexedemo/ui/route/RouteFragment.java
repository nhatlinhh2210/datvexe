package com.example.datvexedemo.ui.route;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.datvexedemo.Model.ListRouteModel;
import com.example.datvexedemo.R;
import com.example.datvexedemo.adapter.ListRouteAdapter;
import com.example.datvexedemo.adapter.ListTicketAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RouteFragment extends Fragment {

    Unbinder unbinder;


    RecyclerView recycler_ticketLookup;

    private RouteViewModel routeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        routeViewModel =
                ViewModelProviders.of(this).get(RouteViewModel.class);
        View root = inflater.inflate(R.layout.fragment_route, container, false);
        recycler_ticketLookup = (RecyclerView) root.findViewById(R.id.recycler_ticketLookup);
        init();
        unbinder = ButterKnife.bind(this, root);

        routeViewModel.getTicketList().observe(this,listTicket -> {
            ListTicketAdapter adapter = new ListTicketAdapter(getContext(),listTicket);
            recycler_ticketLookup.setAdapter(adapter);
        });
        return root;
    }

    private void init() {
        recycler_ticketLookup.setHasFixedSize(true);
        recycler_ticketLookup.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
    }
}

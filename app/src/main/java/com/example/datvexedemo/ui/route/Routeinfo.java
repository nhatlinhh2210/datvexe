package com.example.datvexedemo.ui.route;

import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.datvexedemo.Common.Common;
import com.example.datvexedemo.Model.ListRouteModel;
import com.example.datvexedemo.Model.Ticket;
import com.example.datvexedemo.Model.UserModel;
import com.example.datvexedemo.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class Routeinfo extends Fragment {

    private RouteinfoViewModel routeinfoViewModel;
    Dialog dialog;
    
    Unbinder unbinder;

    NavController navController;

    @BindView(R.id.btn_addTicket)
    Button btn_addTicket;
    
    @BindView(R.id.tv_routeName)
    TextView tv_routeName;

    @BindView(R.id.tv_routeSerial)
    TextView tv_routeSerial;

    @BindView(R.id.tv_routeStart)
    TextView tv_routeStart;

    @BindView(R.id.tv_routeEnd)
    TextView tv_routeEnd;

    @BindView(R.id.tv_seatNumber)
    TextView tv_seatNumber;

    @BindView(R.id.tv_seatType)
    TextView tv_seatType;

    @BindView(R.id.tv_routeTimeStart)
    TextView tv_timeStart;

    @BindView(R.id.tv_routeTimeEnd)
    TextView tv_timeEnd;

    @BindView(R.id.tv_routeDayStart)
    TextView tv_routeDayStart;

    @BindView(R.id.tv_routeDayEnd)
    TextView tv_routeDayEnd;

    @BindView(R.id.tv_routeDistance)
    TextView tv_routeDistance;
    
    @BindView(R.id.tv_routePrice)
    TextView tv_routePrice;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        routeinfoViewModel =
                ViewModelProviders.of(this).get(RouteinfoViewModel.class);
        View root = inflater.inflate(R.layout.routeinfo_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);

        routeinfoViewModel.getMutableLiveDataRoute().observe(this,listRouteModel -> {
            displayInfo(listRouteModel);
        });
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference ticketRef = firebaseDatabase.getReference("Ticket");
        DatabaseReference userRef = firebaseDatabase.getReference("User");
        FirebaseAuth firebaseAuth;


        btn_addTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(getContext());
                dialog.setTitle("Xác nhận thông tin Đặt vé");
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.fragment_booking);
                EditText et_name = dialog.findViewById(R.id.et_Name);
                EditText et_phone = dialog.findViewById(R.id.et_phone);

                TextView tv_routeName = dialog.findViewById(R.id.tv_routeName);
                TextView tv_routeEnd = dialog.findViewById(R.id.tv_routeEnd);
                TextView tv_routeDay = dialog.findViewById(R.id.tv_routeDayStart);
                TextView tv_routePrice = dialog.findViewById(R.id.tv_routePrice);
                TextView tv_routeTime = dialog.findViewById(R.id.tv_routeTimeStart);
                Button btn_Accept = dialog.findViewById(R.id.btn_Accept);
                Button btn_Cancel = dialog.findViewById(R.id.btn_Cancel);

                et_name.setText(new StringBuilder().append(Common.currentUser.getusername()));
                et_phone.setText(new StringBuilder().append(Common.currentUser.getPhone()));

                tv_routeName.setText(new StringBuilder("").append(Common.RouteSelected.getName()));
                tv_routeEnd.setText(new StringBuilder("").append(Common.RouteSelected.getEnd()));
                tv_routeDay.setText(new StringBuilder("").append(Common.RouteSelected.getDayStart()));
                tv_routePrice.setText(new StringBuilder("").append(Common.RouteSelected.getPrice()));
                tv_routeTime.setText(new StringBuilder("").append(Common.RouteSelected.getTimeStart()));

                btn_Accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ticketRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Ticket ticketModel = new Ticket(
                                        et_name.getText().toString(),
                                        tv_routeSerial.getText().toString(),
                                        tv_routeDayStart.getText().toString(),
                                        tv_timeStart.getText().toString(),
                                        tv_routePrice.getText().toString(),
                                        tv_routeStart.getText().toString(),
                                        tv_routeEnd.getText().toString());
                                userRef.child(et_phone.getText().toString()).child("Ticket").child(Common.createTicketNumber()).setValue(ticketModel);
                                Toast.makeText(getActivity(), "Đặt vé thành công", Toast.LENGTH_SHORT).show();
                                dialog.cancel();
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(getActivity(), "Lỗi", Toast.LENGTH_SHORT).show();
                            }
                        });
                        dialog.cancel();
                    }
                });

                btn_Cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                dialog.show();
            }
        });


    }

    private void displayInfo(ListRouteModel listRouteModel) {
        tv_routeName.setText(new StringBuilder(listRouteModel.getName()));
        tv_routeSerial.setText(new StringBuilder(listRouteModel.getSerial()));
        tv_routeStart.setText(new StringBuilder(listRouteModel.getStart()));
        tv_routeEnd.setText(new StringBuilder(listRouteModel.getEnd()));
        tv_seatNumber.setText(new StringBuilder(listRouteModel.getSeat().toString()));
        tv_seatType.setText(new StringBuilder(listRouteModel.getSeatType()));
        tv_timeStart.setText(new StringBuilder(listRouteModel.getTimeStart()));
        tv_timeEnd.setText(new StringBuilder(listRouteModel.getTimeEnd()));
        tv_routeDayStart.setText(new StringBuilder(listRouteModel.getDayStart()));
        tv_routeDayEnd.setText(new StringBuilder(listRouteModel.getDayEnd()));
        tv_routeDistance.setText(new StringBuilder(listRouteModel.getDistance().toString()));
        tv_routePrice.setText(new StringBuilder(listRouteModel.getPrice().toString()));
    }

}

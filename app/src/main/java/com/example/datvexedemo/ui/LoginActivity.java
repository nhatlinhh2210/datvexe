package com.example.datvexedemo.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.datvexedemo.Common.Common;
import com.example.datvexedemo.HomeActivity;
import com.example.datvexedemo.Model.UserModel;
import com.example.datvexedemo.R;
import com.firebase.ui.auth.data.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

public class LoginActivity extends AppCompatActivity {


    EditText phone, password;
    Button btnSignIn;
    private DatabaseReference userRef;
    TextView tvSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        password = (MaterialEditText) findViewById(R.id.edtPassword);
        phone = (MaterialEditText) findViewById(R.id.edtPhone);
        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        tvSignup = findViewById(R.id.tv_Signup);

        tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignupActitity.class));
                finish();
            }
        });

        //Init Firebase
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_user = database.getReference("User");
        userRef = FirebaseDatabase.getInstance().getReference(Common.USER_REFERENCES);
        btnSignIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final ProgressDialog mDialog = new ProgressDialog(LoginActivity.this);
                mDialog.setMessage("Please waiting...");
                mDialog.show();

                table_user.addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        //Check if user not exist in database
                        if (dataSnapshot.child(phone.getText().toString()).exists()) {
                            //Get user information
                            mDialog.dismiss();
                            UserModel user = dataSnapshot.child(phone.getText().toString()).getValue(UserModel.class);
                            if (user.getPassword().equals(password.getText().toString())) {
                                Toast.makeText(LoginActivity.this, "Dang nhap thanh cong!", Toast.LENGTH_SHORT).show();
                                goToHomeActitity(user);
                            } else {
                                Toast.makeText(LoginActivity.this, "Wrong Password!!!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mDialog.dismiss();
                            Toast.makeText(LoginActivity.this, "User not exist in Database", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }




//    private void goToSignUpActivity(){
//        startActivity(new Intent(MainActivity.this, SignupActitity.class));
//        finish();
//    }

    private void goToHomeActitity(UserModel user) {
        Common.currentUser = user;// Luc nao cung can co du lieu truoc khi su dung
        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        finish();
    }
}

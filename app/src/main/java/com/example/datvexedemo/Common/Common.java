package com.example.datvexedemo.Common;

import com.example.datvexedemo.Model.ListBusModel;
import com.example.datvexedemo.Model.ListRouteModel;
import com.example.datvexedemo.Model.UserModel;

import java.util.List;
import java.util.Random;

public class Common {
    public static final String USER_REFERENCES = "User";
    public static final String LIST_BUS_REF = "Bus";
    public static final String LIST_ROUTE_REF = "Route";
    public static UserModel currentUser;
    public static ListBusModel BusSelected;
    public static ListRouteModel RouteSelected;

    public static String createTicketNumber() {
        return new StringBuilder()
                .append(System.currentTimeMillis())
                .append(Math.abs(new Random().nextInt()))
                .toString();
    }
}

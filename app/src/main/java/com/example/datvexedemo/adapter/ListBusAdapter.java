package com.example.datvexedemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.datvexedemo.Common.Common;
import com.example.datvexedemo.Model.ListBusModel;
import com.example.datvexedemo.R;
import com.example.datvexedemo.callback.RecyclerClickListener;
import com.example.datvexedemo.event.BusClick;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ListBusAdapter extends RecyclerView.Adapter<ListBusAdapter.MyViewHolder> {

    Context context;
    List<ListBusModel> listBusModels;

    public ListBusAdapter(Context context, List<ListBusModel> listBusModels) {
        this.context = context;
        this.listBusModels = listBusModels;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context)
        .inflate(R.layout.item_bus_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            holder.tv_busCompany.setText(listBusModels.get(position).getCompany());
            holder.tv_busSerial.setText(listBusModels.get(position).getSerial());
            holder.tv_busName.setText(listBusModels.get(position).getName());

            //event
            holder.setClickListener((view, pos) -> {
                Common.BusSelected = listBusModels.get(pos);
                EventBus.getDefault().postSticky(new BusClick(true,listBusModels.get(pos)));
            });
    }

    @Override
    public int getItemCount() {
        return listBusModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        Unbinder unbinder;

        @BindView(R.id.tv_busCompany)
        TextView tv_busCompany;

        @BindView(R.id.tv_busSerial)
        TextView tv_busSerial;

        @BindView(R.id.tv_busName)
        TextView tv_busName;

        RecyclerClickListener clickListener;

        public void setClickListener(RecyclerClickListener clickListener) {
            this.clickListener = clickListener;
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view){
            clickListener.OnItemClickListener(view,getAdapterPosition());
        }
    }
}

package com.example.datvexedemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.datvexedemo.Model.ListRouteModel;
import com.example.datvexedemo.Model.Ticket;
import com.example.datvexedemo.R;
import com.example.datvexedemo.callback.RecyclerClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ListTicketAdapter extends RecyclerView.Adapter<ListTicketAdapter.MyViewHolder> {
    List<Ticket> listTicket;
    Context context;

    public ListTicketAdapter(Context context,List<Ticket> listTicket){
        this.context = context;
        this.listTicket = listTicket;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_ticket_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_routeTimeStart.setText(listTicket.get(position).getTimeStart());
        holder.tv_routeStart.setText(listTicket.get(position).getStart());
        holder.tv_routeEnd.setText(listTicket.get(position).getEnd());
        holder.tv_routeDayStart.setText(listTicket.get(position).getDayStart());
    }

    @Override
    public int getItemCount() {
        return listTicket.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Unbinder unbinder;

        @BindView(R.id.tv_routeTimeStart)
        TextView tv_routeTimeStart;

        @BindView(R.id.tv_routeStart)
        TextView tv_routeStart;

        @BindView(R.id.tv_routeEnd)
        TextView tv_routeEnd;

        @BindView(R.id.tv_routeDayStart)
        TextView tv_routeDayStart;

        RecyclerClickListener clickListener;

        public void setClickListener(RecyclerClickListener clickListener){
            this.clickListener =clickListener;
        }
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            clickListener.OnItemClickListener(view,getAdapterPosition());

        }
    }
}

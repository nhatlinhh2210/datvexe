package com.example.datvexedemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.datvexedemo.Common.Common;
import com.example.datvexedemo.Model.ListRouteModel;
import com.example.datvexedemo.R;
import com.example.datvexedemo.callback.RecyclerClickListener;
import com.example.datvexedemo.event.RouteClick;

import org.greenrobot.eventbus.EventBus;
import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ListRouteAdapter extends RecyclerView.Adapter<ListRouteAdapter.MyViewHolder> {


    List<ListRouteModel> listRouteModels;
    Context context;
    public ListRouteAdapter(Context context,List<ListRouteModel> listRouteModels){
        this.context = context;
        this.listRouteModels = listRouteModels;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_route_layout, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_routeStart.setText(listRouteModels.get(position).getStart());
        holder.tv_routeEnd.setText(listRouteModels.get(position).getEnd());
        holder.tv_routeTimeStart.setText(listRouteModels.get(position).getTimeStart());
        holder.tv_routeTimeEnd.setText(listRouteModels.get(position).getTimeEnd());
        holder.tv_routePrice.setText(listRouteModels.get(position).getPrice().toString());
        holder.tv_seatNumber.setText(listRouteModels.get(position).getSeat().toString());
        holder.tv_routeDayStart.setText(listRouteModels.get(position).getDayStart());

        //event
        holder.setClickListener((view, pos) -> {
            Common.RouteSelected = listRouteModels.get(pos);
            EventBus.getDefault().postSticky(new RouteClick(true,listRouteModels.get(pos)));
        });
    }

    @Override
    public int getItemCount() {
        return listRouteModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        Unbinder unbinder;

        @BindView(R.id.tv_routeTimeStart)
        TextView tv_routeTimeStart;

        @BindView(R.id.tv_routeTimeEnd)
        TextView tv_routeTimeEnd;

        @BindView(R.id.tv_routeStart)
        TextView tv_routeStart;

        @BindView(R.id.tv_routeEnd)
        TextView tv_routeEnd;

        @BindView(R.id.tv_seatNumber)
        TextView tv_seatNumber;

        @BindView(R.id.tv_routePrice)
        TextView tv_routePrice;

        @BindView(R.id.tv_routeDayStart)
        TextView tv_routeDayStart;


        RecyclerClickListener clickListener;

        public void setClickListener(RecyclerClickListener clickListener) {
            this.clickListener = clickListener;
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view){
            clickListener.OnItemClickListener(view,getAdapterPosition());
        }

    }
}
